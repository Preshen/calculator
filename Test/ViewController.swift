//
//  ViewController.swift
//  Test
//
//  Created by Preshen Naidoo on 2019/08/06.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//

import UIKit



class ViewController: UIViewController {
    
    private lazy var viewModel = CalculatorViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBOutlet private weak var DisplayLabel: UILabel!
    @IBOutlet private weak var TextNumber: UITextField!
    
    @IBAction func DoMultiply(_ sender: UIButton) {
        calculateResult()

    }
    
    private func calculateResult() {
        let num: String = TextNumber.text ?? ""
        
        viewModel.multiply(numberInput: Gun(magazineSize: Int(num) ?? 0))
        self.performSegue(withIdentifier: "ResultSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ResultSegue" {
            if let destination = segue.destination as? SecondViewController {
                destination.answer = String(viewModel.answer())
            }
        }
    }
    
    
}

