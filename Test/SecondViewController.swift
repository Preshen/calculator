//
//  SecondViewController.swift
//  Test
//
//  Created by Preshen Naidoo on 2019/08/07.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak private var Answer: UILabel!
    
    var answer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Answer.text = "The Answer: "+answer
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
