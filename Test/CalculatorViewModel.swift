//
//  CalculatorViewModel.swift
//  Test
//
//  Created by Preshen Naidoo on 2019/08/07.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//

import Foundation

protocol Weapon
{
    var name : String {get set}
    var CanFire : Bool {get set}
    var CanCut : Bool {get set}
}

extension Weapon
{
    var CanFire : Bool {return self is Fireable}
    var CanCut : Bool {return self is Cuttable}
}

protocol Fireable {
    var magazineSize : Int {get set}
}

protocol Cuttable {
    var Weight : Double {get set}
    var Steel : String {get set}
}

struct Gun : Weapon , Fireable
{
    var name = "It Works"
    var CanFire = true
    var CanCut = false
    var magazineSize: Int
}

class CalculatorViewModel {
    
    private var result = 0
    
    func multiply(numberInput: Gun) {
        result = numberInput.magazineSize * 4
      }
    
    func answer() -> Int {
       return result
    }
    
}

